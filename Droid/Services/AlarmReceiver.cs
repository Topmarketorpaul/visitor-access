﻿using System;
using Android.Content;
using MvvmCross.Platform;
using VisitorsAccess.Services;

namespace VisitorsAccess.Droid.Services
{
    [BroadcastReceiver]
    public class AlarmReceiver : BroadcastReceiver
    {
        public override async void OnReceive(Context context, Intent intent)
        {
            Console.WriteLine("Alarm started");
            await Mvx.Resolve<IAppService>().StartFetchingData();
            Console.WriteLine("Alarm ended");
        }
    }
}
