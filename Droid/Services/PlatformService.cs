﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.Net.Wifi;
using Android.Support.V4.App;
using VisitorsAccess.Services;
using Plugin.CurrentActivity;
using Android;
using Android.Views;
using Android.Util;
using Android.Support.Design.Widget;
using Android.App;
using Android.Support.Compat;
using System.Collections.Generic;
using Android.Icu.Util;

namespace VisitorsAccess.Droid.Services
{
    public class PlatformService : IPlatformService
    {
        private MainActivity Activity => CrossCurrentActivity.Current.Activity as MainActivity;

        private PendingIntent pendingIntent;

        public async Task AttemptToConnectWifi(string ssid, string preSharedKey)
        {
            try
            {
                var wifiManager = (WifiManager)Android.App.Application.Context
                        .GetSystemService(Context.WifiService);

                if (!wifiManager.IsWifiEnabled)
                    wifiManager.SetWifiEnabled(true);


                var config = new WifiConfiguration();
                config.Ssid = '"' + ssid + '"';
                config.PreSharedKey = '"' + preSharedKey + '"';

                //var formattedSsid = $"\"{ssid}\"";
                //var formattedPassword = $"\"{preSharedKey}\"";

                //var wifiConfig = new WifiConfiguration
                //{
                //    Ssid = formattedSsid,
                //    PreSharedKey = formattedPassword
                //};

                var addNetwork = wifiManager.AddNetwork(config);

                IList<WifiConfiguration> myWifi = wifiManager.ConfiguredNetworks;


                var network = wifiManager.ConfiguredNetworks
                                         .FirstOrDefault(n => n.Ssid.Contains(ssid));

                if (network == null)
                {
                    Console.WriteLine($"Cannot connect to network: {ssid}");
                    return;
                }

                wifiManager.Disconnect();
                var enableNetwork = wifiManager.EnableNetwork(network.NetworkId, true);
                wifiManager.Reconnect();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot connect to network: {ssid}. Exception: " + ex.Message);

            }


        }

        public async void RequestPermissionSettings()
        {

        }

        public void SetupFetchingTask()
        {
            Calendar calendar = Calendar.GetInstance(Android.Icu.Util.TimeZone.Default);

            calendar.Set(CalendarField.HourOfDay, 8);
            //calendar.Set(CalendarField.Minute, 15);
            //calendar.Set(CalendarField.Second, 0);

            Intent alarmIntent = new Intent(this.Activity, typeof(AlarmReceiver));
            if (pendingIntent == null)
            {
                pendingIntent = PendingIntent.GetBroadcast(this.Activity, 1, alarmIntent, PendingIntentFlags.UpdateCurrent);
            }

            AlarmManager alarmManager = (AlarmManager)this.Activity.GetSystemService(Context.AlarmService);
            alarmManager.SetRepeating(AlarmType.RtcWakeup, calendar.TimeInMillis, AlarmManager.IntervalDay, pendingIntent);
        }
    }

}
