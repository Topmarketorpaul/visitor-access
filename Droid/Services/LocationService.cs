﻿using System;
using System.Collections.Generic;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Location;
using Android.Support.V4.Content;
using MvvmCross.Platform;
using Plugin.CurrentActivity;
using VisitorsAccess.Models.ReturnModels;
using VisitorsAccess.Services;
using Android.Gms.Tasks;
using Android.Widget;
using Android.Util;
using Android.Support.V4.App;
using Android.Support.Design.Widget;
using Android.Locations;

namespace VisitorsAccess.Droid.Services
{
    public class LocationService : Java.Lang.Object, ILocationService, IOnCompleteListener
    {
        private enum PendingGeofenceTask
        {
            ADD, REMOVE, NONE
        }

        private MainActivity Activity => CrossCurrentActivity.Current.Activity as MainActivity;

        protected IAppService appService => Mvx.Resolve<IAppService>();

        protected List<WifiRegion> mWifiRegions => appService.GetWifiRegions();

        private GeofencingClient mGeofencingClient;

        private PendingIntent mGeofencePendingIntent;

        public bool LocationServicesEnabled => CheckPermissions();

        private PendingGeofenceTask mPendingGeofenceTask = PendingGeofenceTask.NONE;

        public void RequestPermission()
        {
            RequestPermissions();
        }

        public async void StartMonitoring(List<WifiRegion> wifiRegions)
        {
            CheckGPS();

            if (wifiRegions.Count > 0)
            {
                StopMonitoring();

                await System.Threading.Tasks.Task.Delay(3000);

                mPendingGeofenceTask = PendingGeofenceTask.ADD;

                mGeofencingClient = null;

                mGeofencingClient = LocationServices.GetGeofencingClient(CrossCurrentActivity.Current.Activity);

                IList<IGeofence> mGeofenceList = new List<IGeofence>();

                foreach (var entry in wifiRegions)
                {
                    if (entry.Inbound)
                    {
                        mGeofenceList.Add(new GeofenceBuilder()
                                      .SetRequestId(entry.GeoFenceId.ToString())
                        .SetCircularRegion(
                                          entry.Latitude,
                            entry.Longitude,
                                          entry.Radius
                        )
                                      .SetExpirationDuration(Geofence.NeverExpire)
                        .SetTransitionTypes(Geofence.GeofenceTransitionEnter)
                        .Build());
                    }
                    else
                    {
                        mGeofenceList.Add(new GeofenceBuilder()
                                      .SetRequestId(entry.GeoFenceId.ToString())
                        .SetCircularRegion(
                                          entry.Latitude,
                            entry.Longitude,
                                          entry.Radius
                        )
                                      .SetExpirationDuration(Geofence.NeverExpire)
                                          .SetTransitionTypes(Geofence.GeofenceTransitionExit)
                        .Build());
                    }

                }



                mGeofencingClient.AddGeofences(GetGeofencingRequest(mGeofenceList), GetGeofencePendingIntent()).AddOnCompleteListener(this);
            }


        }

        public void StopMonitoring()
        {
            if (mGeofencingClient != null)
            {
                mPendingGeofenceTask = PendingGeofenceTask.REMOVE;
                mGeofencingClient.RemoveGeofences(GetGeofencePendingIntent()).AddOnCompleteListener(this);
            }

        }

        private bool CheckPermissions()
        {
            var permissionState = ContextCompat.CheckSelfPermission(CrossCurrentActivity.Current.Activity, Manifest.Permission.AccessFineLocation);
            return permissionState == (int)Permission.Granted;
        }

        private bool CheckIfLocationOpened()
        {
            String provider = Android.Provider.Settings.Secure.GetString(this.Activity.ContentResolver, Android.Provider.Settings.Secure.LocationProvidersAllowed);
            if (provider.Contains("gps") || provider.Contains("network"))
                return true;
            return false;
        }


        void CheckGPS()
        {
            LocationManager service = (LocationManager)this.Activity.GetSystemService(Context.LocationService);
            bool enabled = CheckIfLocationOpened();

            // Check if enabled and if not send user to the GPS settings
            if (!enabled)
            {
                Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                this.Activity.StartActivity(intent);
            }
        }


        private GeofencingRequest GetGeofencingRequest(IList<IGeofence> geofences)
        {
            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
            builder.SetInitialTrigger(GeofencingRequest.InitialTriggerEnter);
            builder.AddGeofences(geofences);
            return builder.Build();
        }

        private PendingIntent GetGeofencePendingIntent()
        {
            if (mGeofencePendingIntent != null)
            {
                return mGeofencePendingIntent;
            }
            var intent = new Intent(this.Activity, typeof(GeofenceTransitionsIntentService));
            mGeofencePendingIntent = PendingIntent.GetService(this.Activity, 0, intent, PendingIntentFlags.UpdateCurrent);
            return mGeofencePendingIntent;
        }

        public void OnComplete(Task task)
        {
            if (task.IsSuccessful)
            {
                var messageId = mPendingGeofenceTask == PendingGeofenceTask.ADD ? Resource.String.geofences_added : Resource.String.geofences_removed;
                Toast.MakeText(this.Activity, this.Activity.GetString(messageId), ToastLength.Short).Show();
            }
            else
            {
                var errorMessage = GeofenceErrorMessages.GetErrorString(this.Activity, task.Exception);
                Log.Warn(typeof(MainActivity).Name, errorMessage);
            }

            mPendingGeofenceTask = PendingGeofenceTask.NONE;
        }

        private void RequestPermissions()
        {
            var shouldProvideRationale = ActivityCompat.ShouldShowRequestPermissionRationale(this.Activity, Manifest.Permission.AccessFineLocation);

            if (shouldProvideRationale)
            {
                Log.Info(typeof(MainActivity).Name, "Displaying permission rationale to provide additional context.");
                // var listener = (View.IOnClickListener)new RequestPermissionsClickListener { Activity = this.Activity };
                ShowSnackbar(Resource.String.permission_rationale, Android.Resource.String.Ok);
            }
            else
            {
                ActivityCompat.RequestPermissions(this.Activity, new[] { Manifest.Permission.AccessFineLocation }, MainActivity.REQUEST_PERMISSIONS_REQUEST_CODE);
            }


        }

        private void ShowSnackbar(int mainTextStringId, int actionStringId)
        {
            Snackbar.Make(this.Activity.FindViewById(Android.Resource.Id.Content),
                          this.Activity.GetString(mainTextStringId),
                    Snackbar.LengthIndefinite)
                    .SetAction(this.Activity.GetString(actionStringId), (obj) =>
                    {
                        ActivityCompat.RequestPermissions(Activity, new[] { Manifest.Permission.AccessFineLocation }, MainActivity.REQUEST_PERMISSIONS_REQUEST_CODE);
                    }).Show();
        }

    }
}
