﻿using System;
using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace VisitorsAccess.Droid
{
    [Activity(
        Label = "Visitors Access"
        , MainLauncher = true
        , Icon = "@drawable/appicon"
        , Theme = "@style/Theme.Splash"
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void TriggerFirstNavigate()
        {
            StartActivity(typeof(MainActivity));
            base.TriggerFirstNavigate();

            Finish();
        }
    }
}
