﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MvvmCross.Forms.Droid.Views;
using HockeyApp.Android;
using FFImageLoading.Svg.Forms;

namespace VisitorsAccess.Droid
{
    [Activity(Label = "Visitor Access", Icon = "@drawable/appicon", Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : MvxFormsAppCompatActivity
    {
        public const int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Window.AddFlags(WindowManagerFlags.TranslucentNavigation);
            Window.AddFlags(WindowManagerFlags.TranslucentStatus);


            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(false);
            var ignore = typeof(SvgCachedImage);
        }

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, "b6f2a3ec7d3d4b8391b06b4b60b78d9f");

        }
    }
}
