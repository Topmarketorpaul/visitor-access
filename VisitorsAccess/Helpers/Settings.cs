﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace VisitorsAccess.Helpers
{
    public class AppSettings
    {
        private static ISettings Settings => CrossSettings.Current;

        #region UserToken

        public static string UserToken
        {
            get => Settings.GetValueOrDefault(nameof(UserToken), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(UserToken), value);
        }

        #endregion

        #region IsFirstTime

        public static bool IsFirstTime
        {
            get => Settings.GetValueOrDefault(nameof(IsFirstTime), true);
            set => Settings.AddOrUpdateValue(nameof(IsFirstTime), value);
        }

        #endregion

        #region RegionsData

        public static string RegionsData
        {
            get => Settings.GetValueOrDefault(nameof(RegionsData), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(RegionsData), value);
        }

        #endregion

        #region LastUpdateDate

        public static string LastUpdateDate
        {
            get => Settings.GetValueOrDefault(nameof(LastUpdateDate), new DateTime(1970, 1, 1).ToString("MM/dd/yyyy"));
            set => Settings.AddOrUpdateValue(nameof(LastUpdateDate), value);
        }

        #endregion


        #region GeoData

        public static string GeoEventData
        {
            get => Settings.GetValueOrDefault(nameof(GeoEventData), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(GeoEventData), value);
        }

        #endregion

    }
}
