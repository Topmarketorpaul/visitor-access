﻿using MvvmCross.Forms.Platform;
using Xamarin.Forms;

namespace VisitorsAccess
{
    public partial class App : MvxFormsApplication
    {
        public const string AppName = "Visitor Access";

        public App()
        {
            InitializeComponent();
        }

        protected async override void OnStart()
        {
            // Handle when your app starts
        }

        protected async override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected async override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
