﻿using System;
using System.Collections.Generic;
using VisitorsAccess.Models.ReturnModels;

namespace VisitorsAccess.Services
{
    public interface ILocationService
    {
        bool LocationServicesEnabled { get; }
        void StartMonitoring(List<WifiRegion> wifiRegions);
        void StopMonitoring();
        void RequestPermission();
        //void StartUpdateLocation(double meter = 20);
        //void StopUpdateLocation();
    }
}
