﻿using System;
using System.Threading.Tasks;
namespace VisitorsAccess.Services
{
    public interface IPlatformService
    {
        void RequestPermissionSettings();

        Task AttemptToConnectWifi(string ssid, string preSharedKey);

        void SetupFetchingTask();
    }

}
