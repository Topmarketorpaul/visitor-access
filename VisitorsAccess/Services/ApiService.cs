﻿using System;
using System.Threading.Tasks;
using VisitorsAccess.Helpers;
using VisitorsAccess.Models.ReturnModels;
using Flurl.Http;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace VisitorsAccess.Services
{
    public class ApiService : IApiService
    {
        public const string REGISTER_ENPOINT = AppConstants.STAGING_URL + "/visitorAccess/Register";

        public const string GETGEOFENCES_ENDPOINT = AppConstants.STAGING_URL + "/visitorAccess/GetGeoFences";

        //public const string GETGEOFENCES_ENDPOINT = "http://localhost:5000/api/values/fetch";

        public const string GEOFENCE_EVENT_ENDPOINT = AppConstants.STAGING_URL + "/visitorAccess/GeoFenceEvent";

        public async Task<List<WifiRegion>> GetGeofences()
        {
            try
            {
                var result = await GETGEOFENCES_ENDPOINT.PostUrlEncodedAsync(new
                {
                    AuthenticationToken = AppConstants.AUTHENTICATION_TOKEN
                }).ReceiveJson<List<WifiRegion>>();
                return result;
                //var json = "[   {       \"GeoFenceId\": 1,      \"TenantId\": 1,        \"CampusId\": 1,        \"Inbound\": true,      \"Latitude\": 30.269641,        \"Longitude\": -97.741833,      \"Radius\": 20,     \"Ssid\": \"SBS\",      \"PreSharedKey\": \"starbucks\" },  {       \"GeoFenceId\": 3,      \"TenantId\": 1,        \"CampusId\": 1,        \"Inbound\": false,     \"Latitude\": 30.269641,        \"Longitude\": -97.741833,      \"Radius\": 10,     \"Ssid\": \"SBS\",      \"PreSharedKey\": \"starbucks\" },  {       \"GeoFenceId\": 5,      \"TenantId\": 1,        \"CampusId\": 1,        \"Inbound\": true,      \"Latitude\": 10.7646217,       \"Longitude\": 106.6557867,     \"Radius\": 300,        \"Ssid\": \"Sinh's iPhone\",     \"PreSharedKey\": \"abc123456\"    },  {       \"GeoFenceId\": 7,      \"TenantId\": 1,        \"CampusId\": 1,        \"Inbound\": true,      \"Latitude\": 10.8262794,       \"Longitude\": 106.6381941,     \"Radius\": 300,        \"Ssid\": \"ahiha\",        \"PreSharedKey\": \"RDQeeDjEznn\"   }]";
                //return JsonConvert.DeserializeObject<List<WifiRegion>>(json);
            }
            catch
            {
                return new List<WifiRegion>();
            }
        }

        public async Task<GeoEventReturnModel> PostGeoFenceEvent(double lat, double lng, int geoFenceId)
        {
            try
            {
                var result = await GEOFENCE_EVENT_ENDPOINT.PostUrlEncodedAsync(new
                {
                    Latitude = lat,
                    Longitude = lng,
                    GeoFenceId = geoFenceId,
                    AuthenticationToken = AppConstants.AUTHENTICATION_TOKEN,
                    UserToken = AppSettings.UserToken
                }).ReceiveJson<GeoEventReturnModel>();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public async Task<RegisterReturnModel> RegisterOrAttemptToLogin(string firstName, string lastName, string phone)
        {
            try
            {
                var result = await REGISTER_ENPOINT.PostUrlEncodedAsync(new
                {
                    FirstName = firstName,
                    LastName = lastName,
                    PhoneNumber = phone,
                    AuthenticationToken = AppConstants.AUTHENTICATION_TOKEN
                }).ReceiveJson<RegisterReturnModel>();
                return result;
            }
            catch
            {
                return null;
            }
        }
    }

    public interface IApiService
    {
        Task<RegisterReturnModel> RegisterOrAttemptToLogin(string firstName, string lastName, string phone);

        Task<List<WifiRegion>> GetGeofences();

        Task<GeoEventReturnModel> PostGeoFenceEvent(double lat, double lng, int geoFenceId);
    }
}
