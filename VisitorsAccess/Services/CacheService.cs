﻿using System;
using System.Diagnostics.Contracts;
namespace VisitorsAccess.Services
{
    public interface ICacheService
    {
        string Title { get; set; }

        string Url { get; set; }
    }

    public class CacheService : ICacheService
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}
