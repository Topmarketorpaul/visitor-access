﻿using System;
using Plugin.LocalNotifications;
using VisitorsAccess.Helpers;
using Flurl.Http;
using System.Diagnostics;
using System.Collections.Generic;
using VisitorsAccess.Models.ReturnModels;
using Newtonsoft.Json;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using System.Threading.Tasks;
using Xamarin.Forms;
using VisitorsAccess.ViewModels;

namespace VisitorsAccess.Services
{
    public interface IAppService
    {
        Task StartFetchingData();

        List<WifiRegion> GetWifiRegions();

        Task<bool> PostEvent(double lat, double lng, int geoFenceId);

    }

    public class AppService : IAppService
    {
        public List<WifiRegion> GetWifiRegions()
        {
            List<WifiRegion> wifiRegions = new List<WifiRegion>();

            if (!String.IsNullOrEmpty(AppSettings.RegionsData))
            {
                wifiRegions = JsonConvert.DeserializeObject<List<WifiRegion>>(AppSettings.RegionsData);
            }

            return wifiRegions;
        }

        public async Task<bool> PostEvent(double lat, double lng, int geoFenceId)
        {
            var result = await Mvx.Resolve<IApiService>().PostGeoFenceEvent(lat, lng, geoFenceId);

            //result = new GeoEventReturnModel()
            //{
            //    Success = true,
            //    QrCodeLink = "http://connect2freewifi.com/qrcodes/daykimball.gif",
            //    HomeLink = "https://www.daykimball.org/",
            //    ContactLink = "https://www.daykimball.org/",
            //    LearnLink = "https://www.daykimball.org/",
            //    LegalLink = "https://www.daykimball.org/"
            //};

            if (result == null)
            {
                AppSettings.GeoEventData = String.Empty;
                MessagingCenter.Send<string>(nameof(DashboardViewModel), nameof(DashboardViewModel));
                return false;

            }

            if (result.Success)
            {
                AppSettings.GeoEventData = JsonConvert.SerializeObject(result);
            }
            else
            {
                AppSettings.GeoEventData = String.Empty;
            }

            MessagingCenter.Send<string>(nameof(DashboardViewModel), nameof(DashboardViewModel));

            return result.Success;
        }

        public async Task StartFetchingData()
        {
            DateTime lastUpdate = DateTime.ParseExact(AppSettings.LastUpdateDate, "MM/dd/yyyy", null);
            var now = DateTime.UtcNow;

            bool doFetchData = false;

            if (lastUpdate.Date.CompareTo(new DateTime(1970, 1, 1)) == 0)
            {
                doFetchData = true;
            }
            else if (now.CompareTo(lastUpdate.Date) > 0)
            {
                if (now.Hour > 12)
                {
                    doFetchData = true;
                }
            }
            if (Device.RuntimePlatform == Device.Android)
            {
                doFetchData = true;
            }
            if (doFetchData)
            {
                AppSettings.LastUpdateDate = now.AddDays(1).ToString("MM/dd/yyyy");
                // var log = Mvx.Resolve<IMvxTrace>();
                // log.Trace(MvxTraceLevel.Diagnostic, "Fetching task:", DateTime.Now.ToString() + ": Start fetching");
                var results = await Mvx.Resolve<IApiService>().GetGeofences();

                if (results.Count > 0)
                {
                    AppSettings.RegionsData = JsonConvert.SerializeObject(results);

                    // log.Trace(MvxTraceLevel.Diagnostic, "Fetching task:", $"Fetched {results.Count}");
                }

                //log.Trace(MvxTraceLevel.Diagnostic, "Fetching task:", DateTime.Now.ToString() + ": End fetching");
            }

        }
    }
}
