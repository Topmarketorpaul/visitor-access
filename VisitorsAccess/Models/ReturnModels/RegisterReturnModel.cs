﻿using System;
namespace VisitorsAccess.Models.ReturnModels
{
    public class RegisterReturnModel
    {
        public bool Success { get; set; }

        public string UserToken { get; set; }
    }
}
