﻿using System;
namespace VisitorsAccess.Models.ReturnModels
{
    public class GeoEventReturnModel
    {
        public bool Success { get; set; }

        public string HomeLink { get; set; }

        public string LearnLink { get; set; }

        public string ContactLink { get; set; }

        public string LegalLink { get; set; }

        public string QrCodeLink { get; set; }
    }
}
