﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using VisitorsAccess.Pages.Base;
using VisitorsAccess.ViewModels;
using MvvmCross.Forms.Views.Attributes;
using FFImageLoading;

namespace VisitorsAccess.Pages
{
    [MvxContentPagePresentation]
    public partial class DashboardPage : BasePage<DashboardViewModel>, IDashboardView
    {
        public DashboardPage()
        {
            InitializeComponent();
        }

        public void LoadQrImage(string url)
        {
            svg.Source = ImageSource.FromUri(new Uri(url));
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            this.ViewModel.View = this;
        }
    }
}
