﻿using System;
using System.Collections.Generic;
using MvvmCross.Forms.Views.Attributes;
using VisitorsAccess.Pages.Base;
using VisitorsAccess.ViewModels;
using Xamarin.Forms;

namespace VisitorsAccess.Pages
{
    [MvxContentPagePresentation]
    public partial class InAppBrowserPage : BasePage<InAppBrowserViewModel>, IInAppBrowserView
    {
        public InAppBrowserPage()
        {
            InitializeComponent();
        }

        public void LoadUrl(string url)
        {
            var urlSource = new UrlWebViewSource();
            urlSource.Url = url;
            webView.Source = urlSource;
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            this.ViewModel.View = this;
        }
    }
}
