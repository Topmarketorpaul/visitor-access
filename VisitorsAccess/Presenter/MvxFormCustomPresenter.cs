﻿using System;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Views;
using MvvmCross.Forms.Platform;
using MvvmCross.Forms.Views;
using MvvmCross.Forms.Views.Attributes;

namespace VisitorsAccess.Presenter
{
    public class PresentationConstantValue
    {
        public const string SHOW_DETAIL_PAGE = "ShowDetailPage";

        public const string WRAP_IN_NAVIGATION_PAGE = "WrapInNavigationPage";

        public const string SHOW_MASTER_PAGE = "ShowMasterPage";

        public const string SHOW_MASTER_DETAIL_ROOT_PAGE = "ShowMasterDetailRootPage";

        public const string CLOSE_MASTER_DETAIL_ROOT_PAGE = "ShowMasterDetailRootPage";

        public const string CLEAR_STACK_AND_SHOW_MASTER_DETAIL_ROOT_PAGE = "ClearStackAndShowMasterDetailRootPage";

        public const string CLEAR_STACK_AND_SHOW_PAGE = "ClearStackAndShowPage";
    }

    public class CloseMasterDetailPageHint : MvxPresentationHint
    {
        public IMvxViewModel ViewModel { get; set; }
    }



    public class MvxFormCustomPresenter : MvxFormsPagePresenter
    {
        #region Constructor

        public MvxFormCustomPresenter(MvxFormsApplication formsApplication
                                      , IMvxViewsContainer viewsContainer = null,
                                      IMvxViewModelTypeFinder viewModelTypeFinder = null,
                                      IMvxViewModelLoader viewModelLoader = null,
                                      Dictionary<Type, MvxPresentationAttributeAction> attributeTypesToActionsDictionary = null) : base(formsApplication, viewsContainer, viewModelTypeFinder, viewModelLoader, attributeTypesToActionsDictionary)
        {
        }

        #endregion

        #region Overrides

        public override void Show(MvxViewModelRequest request)
        {
            if (request.PresentationValues != null)
            {
                if (request.PresentationValues.ContainsKey(PresentationConstantValue.CLEAR_STACK_AND_SHOW_PAGE))
                {
                    var att = this.GetPresentationAttribute(request.ViewModelType) as MvxPagePresentationAttribute;
                    att.NoHistory = true;
                    Show(request, att);
                    return;
                }
            }
            base.Show(request);
        }

        private void Show(MvxViewModelRequest request, MvxBasePresentationAttribute attribute)
        {
            var attributeType = attribute.GetType();

            if (AttributeTypesToActionsDictionary.TryGetValue(
                attributeType,
                out MvxPresentationAttributeAction attributeAction))
            {
                if (attributeAction.ShowAction == null)
                {
                    throw new NullReferenceException($"attributeAction.ShowAction is null for attribute: {attributeType.Name}");
                }

                attributeAction.ShowAction.Invoke(attribute.ViewType, attribute, request);
                return;
            }

            throw new KeyNotFoundException($"The type {attributeType.Name} is not configured in the presenter dictionary");
        }

        #endregion


        #region Method


        #endregion
    }
}
