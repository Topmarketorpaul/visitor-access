﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using VisitorsAccess.Services;
using VisitorsAccess.ViewModels.Base;
using VisitorsAccess.Helpers;
namespace VisitorsAccess.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IApiService ApiService;

        private readonly IPlatformService PlatformService;

        readonly ILocationService LocationService;

        #region Constructor

        public LoginViewModel(IMvxNavigationService navigationService, IDialogService dialogService,
                              IApiService apiService, IPlatformService platformService, ILocationService locationService) : base(navigationService, dialogService)
        {
            this.ApiService = apiService;
            this.PlatformService = platformService;
            this.LocationService = locationService;
        }

        #endregion

        #region Overrides

        public override void ViewAppeared()
        {
            base.ViewAppeared();


        }

        #endregion

        #region Properties

        #region FirstName

        private string mFirstName;
        public string FirstName
        {
            get
            {
                return mFirstName;
            }
            set
            {
                SetProperty(ref mFirstName, value);
            }
        }

        #endregion

        #region LastName

        private string mLastName;
        public string LastName
        {
            get
            {
                return mLastName;
            }
            set
            {
                SetProperty(ref mLastName, value);
            }
        }

        #endregion


        #region Phone

        private string mPhone;
        public string Phone
        {
            get
            {
                return mPhone;
            }
            set
            {
                SetProperty(ref mPhone, value);
            }
        }

        #endregion

        #endregion

        #region Commands

        #region RegisterOrAttemptToLoginCommand

        public IMvxAsyncCommand RegisterOrAttemptToLoginCommand => new MvxAsyncCommand(RegisterOrAttemptToLogin);

        private async Task RegisterOrAttemptToLogin()
        {
            try
            {

                //await ClearStackAndNavigateToPage<DashboardViewModel>();
                //return;
                if (String.IsNullOrEmpty(this.FirstName))
                {
                    await DisplayAlert("Alert", "Please Enter FirstName.", "Ok");

                    return;
                }

                if (String.IsNullOrEmpty(LastName))
                {
                    await DisplayAlert("Alert", "Please Enter LastName.", "Ok");

                    return;
                }


                if (String.IsNullOrEmpty(Phone))
                {
                    await DisplayAlert("Alert", "Please Enter Phone.", "Ok");

                    return;
                }

                if (await CheckPermission())
                {
                    ShowLoading();

                    var data = await this.ApiService.RegisterOrAttemptToLogin(FirstName, LastName, Phone);

                    if (data.Success)
                    {
                        HideLoading();

                        AppSettings.UserToken = data.UserToken;

                        await ClearStackAndNavigateToPage<DashboardViewModel>();
                        //await NavigationService.Navigate<DashboardViewModel>();
                    }
                    else
                    {
                        await DisplayAlert(App.AppName, "Unable to Sign in at the moment, please retry", "Close");

                    }
                }
                else
                {

                }

            }
            catch
            {

                await DisplayAlert(App.AppName, "Unable to Sign in at the moment, please retry", "Close");
            }
            finally
            {
                HideLoading();
            }
        }

        #endregion

        #region ShowInfoCommand

        public IMvxAsyncCommand ShowInfoCommand => new MvxAsyncCommand(ShowInfo);

        private async Task ShowInfo()
        {
            await DisplayAlert(App.AppName, "Registering with this application will grant you complimentary access to the WIFI networks of all participating members.", "OK");
        }

        #endregion

        #endregion

        #region Methods

        public async Task<bool> CheckPermission()
        {
            if (!LocationService.LocationServicesEnabled)
            {
                await DisplayAlert("Location Required", "You have to enable the Location", "Got it");
                if (AppSettings.IsFirstTime)
                {
                    LocationService.RequestPermission();
                    AppSettings.IsFirstTime = false;
                }
                else
                {
                    PlatformService.RequestPermissionSettings();
                }
                return false;
            }

            return true;
        }


        #endregion
    }
}
