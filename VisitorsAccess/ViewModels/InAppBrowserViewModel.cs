﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using VisitorsAccess.Services;
using VisitorsAccess.ViewModels.Base;
namespace VisitorsAccess.ViewModels
{
    public interface IInAppBrowserView
    {
        void LoadUrl(string url);
    }

    public class InAppBrowserViewModel : BaseViewModel
    {
        public IInAppBrowserView View { get; set; }

        readonly ICacheService CacheService;

        public InAppBrowserViewModel(IMvxNavigationService navigationService,
                                     IDialogService dialogService, ICacheService cacheService) : base(navigationService, dialogService)
        {
            this.CacheService = cacheService;
        }

        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                SetProperty(ref _title, value);
            }
        }

        public override Task Initialize()
        {
            this.Title = this.CacheService.Title;
            return base.Initialize();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();

            View?.LoadUrl(CacheService.Url);
        }


    }
}
