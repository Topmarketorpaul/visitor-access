﻿using System;
using MvvmCross.Core.Navigation;
using Plugin.LocalNotifications;
using VisitorsAccess.Services;
using VisitorsAccess.ViewModels.Base;
using VisitorsAccess.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;
using VisitorsAccess.Models.ReturnModels;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using Xamarin.Forms;

namespace VisitorsAccess.ViewModels
{
    public interface IDashboardView
    {
        void LoadQrImage(string url);
    }

    public class DashboardViewModel : BaseViewModel
    {
        public IDashboardView View { get; set; }

        readonly IPlatformService PlatformService;

        readonly IAppService AppService;

        readonly ILocationService LocationService;

        readonly ICacheService CacheService;


        public DashboardViewModel(IMvxNavigationService navigationService, IDialogService dialogService,
                                  IPlatformService platformService, IAppService appService,
                                  ILocationService locationService, IApiService apiService, ICacheService cacheService) : base(navigationService, dialogService)
        {
            this.PlatformService = platformService;
            this.AppService = appService;
            this.LocationService = locationService;
            this.CacheService = cacheService;

            MessagingCenter.Subscribe<string>(nameof(DashboardViewModel), nameof(DashboardViewModel), UpdateData);
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();


            Load();
            //LoadTest();
        }

        async void LoadTest()
        {
            var result = await AppService.PostEvent(-19.123279487, 31.283874874, 3);

            UpdateData(nameof(DashboardViewModel));
        }

        public override Task Initialize()
        {


            return base.Initialize();
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            MessagingCenter.Unsubscribe<string>(nameof(DashboardViewModel), nameof(DashboardViewModel));
            base.ViewDestroy(viewFinishing);
        }

        #region Properties

        GeoEventReturnModel GeoEventReturnModel { get; set; }

        private bool _showQrCode;
        public bool ShowQrCode
        {
            get
            {
                return _showQrCode;
            }
            set
            {
                SetProperty(ref _showQrCode, value);
            }
        }

        #endregion

        #region Methods

        async void Load()
        {
            this.PlatformService.SetupFetchingTask();

            await this.AppService.StartFetchingData();
            //this.LocationService.StopMonitoring();
            this.LocationService.StartMonitoring(this.AppService.GetWifiRegions());
            //this.LocationService.StartMonitoring(new List<WifiRegion>());
        }

        void UpdateData(string tag)
        {

            if (!String.IsNullOrEmpty(AppSettings.GeoEventData))
            {
                var geodata = JsonConvert.DeserializeObject<GeoEventReturnModel>(AppSettings.GeoEventData);
                GeoEventReturnModel = geodata;
            }
            else
            {
                GeoEventReturnModel = new GeoEventReturnModel();
            }

            if (GeoEventReturnModel.Success)
            {
                View?.LoadQrImage(this.GeoEventReturnModel.QrCodeLink);
            }
            ShowQrCode = GeoEventReturnModel.Success;


        }

        #endregion

        #region ShowInfoCommand

        public IMvxAsyncCommand ShowInfoCommand => new MvxAsyncCommand(ShowInfo);

        private async Task ShowInfo()
        {
            await DisplayAlert(App.AppName, "Registering with this application will grant you complimentary access to the WIFI networks of all participating members.", "OK");
        }

        #endregion

        #region TapLinkCommand

        public IMvxAsyncCommand<int> TapLinkCommand => new MvxAsyncCommand<int>(TapLink);

        private async Task TapLink(int param)
        {
            if (param == 1)
            {
                CacheService.Title = "Home";
                CacheService.Url = this.GeoEventReturnModel.HomeLink;
                await this.NavigationService.Navigate<InAppBrowserViewModel>();
            }
            else if (param == 2)
            {
                CacheService.Title = "Learn More";
                CacheService.Url = this.GeoEventReturnModel.LearnLink;
                await this.NavigationService.Navigate<InAppBrowserViewModel>();
            }
            else if (param == 3)
            {
                CacheService.Title = "Contact";
                CacheService.Url = this.GeoEventReturnModel.ContactLink;
                await this.NavigationService.Navigate<InAppBrowserViewModel>();
            }
            else if (param == 4)
            {
                CacheService.Title = "Legal";
                CacheService.Url = this.GeoEventReturnModel.LegalLink;
                await this.NavigationService.Navigate<InAppBrowserViewModel>();

            }
        }

        #endregion
    }
}
