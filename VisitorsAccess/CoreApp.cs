﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using VisitorsAccess.Services;
using VisitorsAccess.ViewModels;
using VisitorsAccess.Helpers;

namespace VisitorsAccess
{
    public class CoreApp : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            if (String.IsNullOrEmpty(AppSettings.UserToken))
            {
                RegisterNavigationServiceAppStart<LoginViewModel>();
            }
            else
            {
                RegisterNavigationServiceAppStart<DashboardViewModel>();
            }



            Mvx.RegisterType<IDialogService, DialogService>();

            Mvx.RegisterType<IApiService, ApiService>();

            Mvx.RegisterSingleton<IAppService>(new AppService());
            Mvx.RegisterSingleton<ICacheService>(new CacheService());

        }
    }
}
