﻿using System;
using MvvmCross.Platform;
using UserNotifications;
using VisitorsAccess.Services;

namespace VisitorsAccess.iOS
{
    public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            // Tell system to display the notification anyway or use
            // `None` to say we have handled the display locally.

            //Console.WriteLine("Notification " + DateTime.Now.ToString());
            //Mvx.Resolve<IAppService>().StartFetchingData();

            completionHandler(UNNotificationPresentationOptions.None);


        }
    }
}
