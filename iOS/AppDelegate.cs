﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FFImageLoading.Svg.Forms;
using Foundation;
using HockeyApp.iOS;
using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.iOS;
using MvvmCross.Platform;
using ObjCRuntime;
using UIKit;
using UserNotifications;
using VisitorsAccess.Helpers;
using VisitorsAccess.Services;

namespace VisitorsAccess.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxFormsApplicationDelegate
    {
        private const double MINIMUM_BACKGROUND_FETCH_INTERVAL = 900;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(65, 105, 225);
            UINavigationBar.Appearance.TintColor = UIColor.FromRGB(255, 255, 255);


            var setup = new Setup(this, Window);
            setup.Initialize();

            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();

            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();

            var ignore = typeof(SvgCachedImage);

            LoadApplication(setup.FormsApplication);

            Window.MakeKeyAndVisible();


            StartBackgroundFetching();


            var manager = BITHockeyManager.SharedHockeyManager;
            manager.Configure("34625a2aa7ab404692af1496c6fd0fe6");
            manager.StartManager();
            manager.Authenticator.AuthenticateInstallation();

            return base.FinishedLaunching(app, options);
        }

        public async override void PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
        {
            try
            {
                await Mvx.Resolve<IAppService>().StartFetchingData();
                completionHandler(UIBackgroundFetchResult.NewData);
            }
            catch
            {

                completionHandler(UIBackgroundFetchResult.Failed);
            }
        }



        #region Methods


        void StartBackgroundFetching()
        {
            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(MINIMUM_BACKGROUND_FETCH_INTERVAL);
        }


        #endregion
    }
}
