﻿using System;
using CoreLocation;
using UIKit;
using VisitorsAccess.Services;
using System.Diagnostics;
using System.Collections.Generic;
using VisitorsAccess.Models.ReturnModels;
using System.Linq;
using MvvmCross.Platform;
using System.Threading.Tasks;
using NetworkExtension;

namespace VisitorsAccess.iOS.Services
{
    public class LocationService : ILocationService
    {
        protected CLLocationManager mLocMgr;

        protected IAppService appService => Mvx.Resolve<IAppService>();

        protected List<WifiRegion> mWifiRegions => appService.GetWifiRegions();


        public CLLocationManager LocMgr
        {
            get { return this.mLocMgr; }
        }

        public bool LocationServicesEnabled => CLLocationManager.Status == CLAuthorizationStatus.AuthorizedAlways;

        public void StartMonitoring(List<WifiRegion> wifiRegions)
        {
            RequestLocationMonitor();

            if (LocationServicesEnabled)
            {
                if (CLLocationManager.Status != CLAuthorizationStatus.Denied)
                {
                    if (CLLocationManager.IsMonitoringAvailable(typeof(CLCircularRegion)))
                    {
                        StopMonitoring();

                        if (wifiRegions.Count > 0)
                        {
                            LocMgr.DidStartMonitoringForRegion += LocMgr_DidStartMonitoringForRegion;
                            LocMgr.RegionEntered += LocMgr_RegionEntered;
                            LocMgr.RegionLeft += LocMgr_RegionLeft;
                            LocMgr.DidDetermineState += LocMgr_DidDetermineState;
                            LocMgr.MonitoringFailed += LocMgr_MonitoringFailed;

                            foreach (var item in wifiRegions)
                            {
                                CLCircularRegion region = new CLCircularRegion(new CLLocationCoordinate2D(item.Latitude, item.Longitude), item.Radius, item.GeoFenceId.ToString());

                                if (!LocMgr.MonitoredRegions.Contains(region))
                                {
                                    LocMgr.StartMonitoring(region);
                                }
                                //else
                                //{
                                //    LocMgr.RequestState(region);
                                //}
                            }
                        }
                        else
                        {

                            StartUpdateLocation();
                        }

                    }

                }
            }
        }

        public void StopMonitoring()
        {
            if (CLLocationManager.IsMonitoringAvailable(typeof(CLCircularRegion)))
            {
                foreach (CLCircularRegion region in LocMgr.MonitoredRegions)
                {
                    LocMgr.StopMonitoring(region);
                }
                LocMgr.DidStartMonitoringForRegion -= LocMgr_DidStartMonitoringForRegion;
                LocMgr.RegionEntered -= LocMgr_RegionEntered;
                LocMgr.RegionLeft -= LocMgr_RegionLeft;
                LocMgr.DidDetermineState += LocMgr_DidDetermineState;
            }
        }

        public void RequestPermission()
        {
            RequestLocationMonitor();
        }

        public void StartUpdateLocation()
        {
            if (LocationServicesEnabled)
            {
                this.LocMgr.DesiredAccuracy = CLLocation.AccuracyHundredMeters;
                this.LocMgr.LocationsUpdated += LocMgr_LocationsUpdated;
                this.LocMgr.StartUpdatingLocation();
            }
        }

        public void StopUpdateLocation()
        {
            if (LocationServicesEnabled)
            {
                this.LocMgr.StopUpdatingLocation();
                this.LocMgr.LocationsUpdated -= LocMgr_LocationsUpdated;
            }
        }

        #region Events

        async void LocMgr_DidDetermineState(object sender, CLRegionStateDeterminedEventArgs e)
        {
            Console.WriteLine("State " + e.State + "  " + e.Region.ToString());
            if (e.State == CLRegionState.Inside)
            {

                var wifiRegion = mWifiRegions.FirstOrDefault(wf => wf.GeoFenceId.ToString().Equals(e.Region.Identifier));
                if (wifiRegion != null && wifiRegion.Inbound)
                {
                    // await Mvx.Resolve<IDialogService>().ShowMessage("Entered " + e.Region.Identifier);
                    Console.WriteLine("Entered " + e.Region.ToString());
                    Task.Run(async () =>
                   {
                       await AttemptToConnectWifi(wifiRegion.Ssid, wifiRegion.PreSharedKey);
                       var result = await appService.PostEvent(wifiRegion.Latitude, wifiRegion.Longitude, wifiRegion.GeoFenceId);

                       Console.WriteLine(result);
                   });
                }
            }
        }

        void LocMgr_MonitoringFailed(object sender, CLRegionErrorEventArgs e)
        {

            Console.WriteLine("Failed  region {0}", e.Error.DebugDescription);
        }

        async void LocMgr_DidStartMonitoringForRegion(object sender, CLRegionEventArgs e)
        {
            Console.WriteLine("Now monitoring region {0}", e.Region.ToString());

            await Task.Delay(2000);

            LocMgr.RequestState(e.Region);
        }

        async void LocMgr_RegionLeft(object sender, CLRegionEventArgs e)
        {
            var wifiRegion = mWifiRegions.FirstOrDefault(wf => wf.GeoFenceId.ToString().Equals(e.Region.Identifier));
            if (wifiRegion != null && !wifiRegion.Inbound)
            {
                Console.WriteLine("Just left " + e.Region.ToString());

                //await Mvx.Resolve<IDialogService>().ShowMessage("Just left " + e.Region.Identifier);
                await Task.Run(async () =>
                {
                    var result = await appService.PostEvent(wifiRegion.Latitude, wifiRegion.Longitude, wifiRegion.GeoFenceId);

                    Console.WriteLine(result);
                });
            }

        }


        async void LocMgr_RegionEntered(object sender, CLRegionEventArgs e)
        {
            var wifiRegion = mWifiRegions.FirstOrDefault(wf => wf.GeoFenceId.ToString().Equals(e.Region.Identifier));
            if (wifiRegion != null && wifiRegion.Inbound)
            {
                Console.WriteLine("Just entered " + e.Region.ToString());
                //await Mvx.Resolve<IDialogService>().ShowMessage("Just entered " + e.Region.Identifier);
                await Task.Run(async () =>
                {
                    await AttemptToConnectWifi(wifiRegion.Ssid, wifiRegion.PreSharedKey);
                    var result = await appService.PostEvent(wifiRegion.Latitude, wifiRegion.Longitude, wifiRegion.GeoFenceId);

                    Console.WriteLine(result);
                });
            }

        }

        void LocMgr_LocationsUpdated(object sender, CLLocationsUpdatedEventArgs e)
        {
            var location = e.Locations.Last();
            Console.WriteLine($"Location Updated! {location.Coordinate.Latitude} {location.Coordinate.Longitude}");
            if (this.mWifiRegions.Count > 0)
            {
                StopUpdateLocation();
                StartMonitoring(this.mWifiRegions);
            }

        }



        #endregion

        #region Methods

        void RequestLocationMonitor()
        {
            this.mLocMgr = new CLLocationManager();
            this.mLocMgr.PausesLocationUpdatesAutomatically = false;

            // iOS 8 has additional permissions requirements
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                mLocMgr.RequestAlwaysAuthorization();
                mLocMgr.RequestWhenInUseAuthorization();
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
            {
                mLocMgr.AllowsBackgroundLocationUpdates = true;
            }
        }

        public async Task AttemptToConnectWifi(string ssid, string preSharedKey)
        {
            try
            {
                //var config = new NEHotspotConfiguration(ssid, preSharedKey, false) { JoinOnce = true };
                //var configManager = new NEHotspotConfigurationManager();
                //await configManager.ApplyConfigurationAsync(config);

                //Console.WriteLine("Connected!!!!!");

                var config = new NEHotspotConfiguration(ssid, preSharedKey, false) { };


                NEHotspotConfigurationManager.SharedManager.ApplyConfiguration(config, (obj) =>
                 {
                     if (obj == null)
                     {
                         Console.WriteLine("Connected");
                     }
                     else
                     {
                         Console.WriteLine(obj.LocalizedDescription);
                     }
                 });
            }
            catch (Foundation.NSErrorException error)
            {
                Console.WriteLine(error.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        #endregion

    }
}
