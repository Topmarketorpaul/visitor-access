﻿
using System;
using System.Threading.Tasks;
using Foundation;
using NetworkExtension;
using UIKit;
using UserNotifications;
using VisitorsAccess.Services;
namespace VisitorsAccess.iOS.Services
{
    public class PlatformService : IPlatformService
    {
        public async Task AttemptToConnectWifi(string ssid, string preSharedKey)
        {
            try
            {
                var config = new NEHotspotConfiguration(ssid, preSharedKey, false) { JoinOnce = true };
                var configManager = new NEHotspotConfigurationManager();
                await configManager.ApplyConfigurationAsync(config);

                Console.WriteLine("Connected!!!!!");

            }
            catch (Foundation.NSErrorException error)
            {
                Console.WriteLine(error.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RequestPermissionSettings()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("app-settings:"));
        }

        public void SetupFetchingTask()
        {
            //ignore for iOS
            // throw new NotImplementedException();
        }
    }
}
